<?php

class IPLocation
{
    private string $apiKey = '';
    private array $fields = ['city', 'state_prov'];
    private string $lang = 'ru';
    private string $excludes = '';
    private string $include = '';

    /**
     * @param string $apiKey
     */
    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function addFields(array $fields)
    {
        $this->fields = array_unique([...$this->fields, ...$fields]);
    }

    private function getFieldsStr(): string
    {
        return implode(',', $this->fields);
    }

    public function getLocationData($ip)
    {
        $fields = $this->getFieldsStr();

        $url =
            "https://api.ipgeolocation.io/ipgeo?apiKey="
            .$this->apiKey
            ."&ip=".$ip
            ."&lang="
            .$this->lang
            ."&fields="
            .$fields
            ."&excludes="
            .$this->excludes
            ."&include="
            .$this->include;

        $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_URL, $url);
        curl_setopt($cURL, CURLOPT_HTTPGET, true);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json'
        ));

        return json_decode(curl_exec($cURL), true);
    }


}
