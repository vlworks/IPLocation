# IPLocation
Использует [API](https://app.ipgeolocation.io/) для получения данных о пользователе по IP.

    $apiKey = "your_api_key";
    $location = new IPLocation($apiKey); // return assoc array[ 'city' => 'city']

## Настройки по умолчанию
### Массив возвращаемых полей от API
`private array $fields = ['city', 'state_prov'];`

Добавить поля:

`$location->addFields['latitude', 'longitude']`
### Язык по умолчанию
`private string $lang = 'ru';`